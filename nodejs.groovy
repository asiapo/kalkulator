job ('test code job')
{
    scm {
        git('https://gitlab.com/asiapo/kalkulator.git') { node ->
        node / gitConfigName('DSL script')
        node / gitConfigEmail('jenkins-dsl-script@altkom.com')
    }       
    }
    triggers {
        scm('H/5 * * * *')
    }
    wrappers {
        nodejs('nodejs')
    }
    steps {
        shell('node calc.js')
    }

}